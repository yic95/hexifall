# 專有詞語

- 物件
  - `Hiex, Hiexes, H, hs`、`六角`：形如平頂六邊形的物件
  - `Hexi, Hexies, Hx, Hxs`、`聚六角`：形如多個平頂六邊形**邊對邊接合而成**的物件

- 物件的狀態
  - `Player-controlled, PC`、`受控的`：形容Hexi受玩家控制並可旋轉
  - `Self-moving, SM`、`自動的`：形容Hexi正在自行（不受玩家控制地）移動
  - `Stable, S`、`穏定的`：形容Hexi的下一步不是向下移一格。只能用來形容受控的Hexi
  - `Stopped, T`、`受阻的`：形容Hexi的下一步是停在原地不動。

- 操作
  - `Stablize, s`、`安定`：使Stable Hexi失去玩家控制
  - `Lock, l`、`鎖定`：使Hexi固定在盤上，無法再移動，轉化為一個個Hiexes

# 盤的更新

大概過程如下：

1. 嘗試生成Hexi
  1. 如果失敗，遊戲結束
2. 更新每一個Hexi
  1. 嘗試下移
  2. 嘗試右移
  3. 嘗試左移
  4. （如果SM）鎖定（盤的更新不會鎖定PC Hxs）
3. 如果PC Hexi為S，把`has_stable_player_controlled_hexi`設為True
5. 如果`is_player_controlled_hexi_just_locked`（通常是Timing Manager設的，它負責呼叫`lock_pc_hexies`），開始清除流程，回到 (1)
4. 回到 (2)

# 實作

## Hexi座標(x, y)轉成盤座標(x', y')，(1, 1)為原點

- 相錯時下移
  (x', y') = (x, floor(y/2) + 1)
- 相錯時上移
  (x', y') = (x, ceil(y/2))