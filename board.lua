---@alias vec2 {[1]: integer, [2]: integer}

require("hexi")

---Convert visual matrix sizes to actual ones.
---@param visual_size vec2
---@return vec2
function visual_to_matrix_size(visual_size)
  return {visual_size[1], visual_size[2] + math.floor(visual_size[1] / 2)}
end

---Check whether a given coordinates (one-indexed) is on board
---@param x integer x coordinate (one-indexed)
---@param y integer y coordinate (one-indexed)
---@param mw integer matrix width
---@param mh integer matrix height
---@return boolean
function is_position_on_board(x, y, mw, mh)
  if x < 1 or y < 1 or mw < 1 or mh < 1 or mw < x or mh < y then
    return false
  end

  local upper_check = y > math.floor(mw / 2) - (math.ceil(x / 2) - 1)
  local lower_check = y < (mh - math.floor(mw / 2) + 1) + math.ceil((mw - x) / 2)

  return upper_check and lower_check
end

---Get coordinate in nth visual layer
---@param n integer
---@param mw integer
---@return vec2[]
function get_nth_visual_layer(n, mw)
  local result = {}  ---@type vec2[]
  local padding = math.floor(mw / 2)
  for i = 1, mw, 1 do
    result[i] = {i, padding + n - (math.ceil(i / 2) - 1)}
  end
  return result
end

---Return layers fully filled listed from top to bottom
---@return integer[]
function Board:list_layers_fully_filled()
  local layers_fully_filled = {}
  for n = 1, self.visual_size[2] do
    local is_layer_cleared = true
    for _, coordinates in pairs(self.visual_layer_lookup[n]) do
      if self.matrix[coordinates[2]][coordinates[1]] == 0 then
        is_layer_cleared = false
        break
      end
    end

    if is_layer_cleared then
      layers_fully_filled[#layers_fully_filled + 1] = n
    end
  end

  return layers_fully_filled
end

---@class Board
---@field protected id integer
---@field visual_size vec2 Read-only. Number of columns and layers.
---@field moving_hexies Hexi[] List of self-moving hexies
---@field matrix integer[][] 0: empty; 1: not on board
---@field matrix_size vec2 Read-only. Horizontal and vertical length of the matrix
---@field cell_occupy_state boolean[][] Matrix showing whether a cell on board has been occupied or not
---@field visual_layer_lookup vec2[][] Read-only. Lists of coordinates of layers
---@field top_onscreen_layer integer Read-only. The top (smallist layer id) layer that is not in the buffer zone
---@field fully_filled_layers integer[] Fully-filled layers listed from bottom to top
---@field self_moving_hexi_just_lock boolean Whether more than one self-moving hexi lock on this frame
Board = {}

---Create new board
---@param id integer
---@param vw integer Visual width
---@param vh integer Visual height. Being doubled automatically.
---@return Board
function Board:new(id, vw, vh)
  -- Create matrix and visual_layer_table
  vh = vh * 2
  local matrix_size = visual_to_matrix_size({vw, vh})
  local matrix = {}
  local cell_occupy_state = {}
  for i = 1, matrix_size[1] do
    matrix[i] = {}
    cell_occupy_state[i] = {}
    for j = 1, matrix_size[2] do
      matrix[i][j] = 1
      cell_occupy_state[i][j] = false
    end
  end

  -- Initialize visual_layer_lookup and matrix
  local visual_layer_lookup = {}
  for n = 1, vh, 1 do
    local nth_layer = get_nth_visual_layer(n, matrix_size[1])
    visual_layer_lookup[#visual_layer_lookup+1] = get_nth_visual_layer(n, matrix_size[1])
    for _, layer in pairs(nth_layer) do
      matrix[layer[2]][layer[1]] = 0
    end
  end

  local obj =  {
    id = id,
    visual_size = {vw, vh},
    player_controlled_hexies = {},
    moving_hexies = {},
    matrix = matrix,
    matrix_size = matrix_size,
    visual_layer_lookup = visual_layer_lookup,
    top_onscreen_layer = vh / 2 + 1,
    fully_filled_layers = {},
  }

  setmetatable(obj, {__index=Board})
  return obj
end

---Move layers on matrix of `board` that have id equal to or less than `bottom_layer` by vertically `move`.
---Return true if the move causes some hexi on matrix be pushed out of bound.
---@param bottom_layer integer The layer of the highest id to be affected
---@param move integer Negative for moving down.
---@return boolean
function Board:move_layers(bottom_layer, move)
  -- No move
  if move == 0 then return false end

  -- Move up
  if move > 0 then
    for l = move, bottom_layer, -1 do
      for _, c in pairs(self.visual_layer_lookup[l]) do
        self.matrix[c[2] + move][c[1]] = self.matrix[c[2]][c[1]]
      end
    end
    -- Check for overflow.
    for l = move - 1, 1, -1 do
      for _, c in pairs(self.visual_layer_lookup[l]) do
        if self.matrix[c[2]][c[1]] ~= 0 then
          return true
        end
      end
    end
    return false
  end

  -- Move down, move < 0
  if move < 0 then
    local is_overwrie = false
    -- Check for overwrite
    for l = self.visual_size[2], bottom_layer - move, -1 do
      for _, c in pairs(self.visual_layer_lookup[l]) do
        if self.matrix[c[2]][c[1]] ~= 0 then
          is_overwrie = true
        end
      end
    end

    for l = bottom_layer - move - 1, 1, -1 do
      for _, c in pairs(self.visual_layer_lookup[l]) do
        self.matrix[c[2] + move][c[1]] = self.matrix[c[2]][c[1]]
      end
    end
    return is_overwrie
---@diagnostic disable-next-line: missing-return
  end
end

---Clear fully-filled layers and move layers on top of them down
function Board:collapse()
  local layer_count_accumulator = 0
  local last_fully_filled_layer = self.fully_filled_layers[1] - 1
  for _, layer in pairs(self.fully_filled_layers) do
    if layer == last_fully_filled_layer + 1 then
      layer_count_accumulator = layer_count_accumulator + 1
    else
      self:move_layers(last_fully_filled_layer, layer_count_accumulator)
      layer_count_accumulator = 1
    end

    last_fully_filled_layer = layer
  end
end

---Add a self moving hexi to the board
---@param shape boolean[][]
---@param position halfVec2 Hexi coordinate system, aligned position only
---@param type_id integer
function Board:add_self_moving_hexi(shape, position, type_id)
  for i = #shape, 1, -1 do  -- remove lower empty row
    local is_row_blank = true
    for j = 1, #shape[1] do
      if shape[i][j] then
        is_row_blank = false
      end
    end
    if not is_row_blank then
      break
    end
    shape[i] = nil
  end
  -- And upper
  local upper_blank_row_count = 0
  for i = 1, #shape do
    local is_row_blank = true
    for j = 1, #shape[1] do
      if shape[i][j] then
        is_row_blank = false
        break
      end
    end
    if not is_row_blank then
      break
    end
    upper_blank_row_count = upper_blank_row_count + 1
  end
  for i = 1, #shape - upper_blank_row_count do
    shape[i] = shape[i] + upper_blank_row_count
  end

  for j =  #shape[1], 1, -1 do  -- Remove right
    local is_col_clear = true
    for i = 1, #shape do
      if shape[i][j] then
        is_col_clear = false
        break
      end
    end
    if not is_col_clear then
      break
    end
    for i = 1, #shape do
      shape[i][j] = nil
    end
  end
  -- And left
  local left_empty_column_empty_count = 0
  for j = #shape[1], 1, -1 do
    local is_column_empty = true
    for i = 1, #shape do
      if shape[i][j] then
        is_column_empty = false
        break
      end
    end
    if not is_column_empty then
      break
    end
    left_empty_column_empty_count = left_empty_column_empty_count + 1
  end
  for i = 1, #shape do
    for j = 1, #shape[1] - left_empty_column_empty_count do
      shape[i][j] = shape[i][j+left_empty_column_empty_count]
    end
  end

  for i = 1, #self.moving_hexies do
    if not self.moving_hexies[i].exists then
      self.moving_hexies[i]:assign(self.cell_occupy_state, shape, position, type_id)
      return
    end
  end
  self.moving_hexies[#self.moving_hexies+1] = Hexi:new(self.cell_occupy_state, shape, position, type_id)
end

---Add Hiex (locked Hexi) to the matrix of this board
---@param shape boolean[][]
---@param position vec2
---@param type_id integer
function Board:add_hiexes(shape, position, type_id)
  for y, row in ipairs(shape) do
    for x, cell in ipairs(row) do
      if cell then
        self.matrix[y + position[2]][x + position[1]] = type_id
      end
    end
  end
end

function Board:update()
  for i = 1, #self.cell_occupy_state do
    for j = 1, #self.cell_occupy_state[i] do
      self.cell_occupy_state[i][j] = self.matrix[i][j] ~= 0
    end
  end
  local smhxs_just_lock = false
  for _, hexi in ipairs(self.moving_hexies) do
    if self.self_moving_hexi_just_lock then
      hexi:update(self.matrix, true)
    else
      hexi:update(self.matrix, false)
    end
    if hexi.next_update_move == 0 then  -- Lock stable self-moving hexies
      for y, row in ipairs(hexi.shape) do
        for x, cell in ipairs(row) do
          if cell then
            self.matrix[y][x] = hexi.type_id
          end
        end
      end
      hexi.exists = false
      smhxs_just_lock = true
    end
    for y, row in ipairs(hexi.shape) do
      for x, cell in ipairs(row) do
        self.cell_occupy_state[y + hexi.position[2]][x + hexi.position[1]] = cell
      end
    end
  end
  self.self_moving_hexi_just_lock = smhxs_just_lock
end

