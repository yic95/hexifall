---@alias halfVec2 {[1]: integer, [2]: integer}

---@alias HexiAutoMove
---| 0 # Stay in place
---| 1 # Slide left
---| 2 # Move down
---| 3 # Slide right

---@alias HexiRotation
---| 0 # No rotation
---| 1 # CW once
---| 2 # CW twice
---| 3 # 180 deg
---| 4 # CCW twice
---| 5 # CCW once

---@class HexiType
---@field id integer
---@field rotation_table {[1]: orientation_bitmap, [2]: orientation_bitmap, [3]: orientation_bitmap, [4]: orientation_bitmap, [5]: orientation_bitmap, [6]: orientation_bitmap}

---@class orientation_bitmap
---@field rotation_tests vec2[]
---@field graph_lowset_position vec2
---@field graph_center_column integer
---@field bitmap boolean[][]

---@class Hexi
---@field exists boolean Whether this hexi exists or not
---@field position halfVec2 Position on matrix
---@field shape boolean[][]
---@field type_id integer
---@field next_update_move HexiAutoMove The move this Hexi will take in the next update
Hexi = {}

---@param shape boolean[]
---@param position vec2
---@return Hexi
function Hexi:new(matrix, shape, position, type_id)
  local obj = {}
  setmetatable(obj, {__index=Hexi})
  obj:assign(matrix, shape, position, type_id)
  return obj
end

---@class PlayerControlledHexi
---@field exists boolean Whether this hexi exists or not
---@field position halfVec2 Position on matrix
---@field orientation integer Index to hexi_type's rotation_table
---@field hexi_type HexiType
---@field reset_column integer
---@field reset_layer vec2[]
---@field is_stable boolean
---@field should_lock boolean
PlayerControlledHexi = {}

function PlayerControlledHexi:new(hexi_type, column, floor, orientation)
  local obj = {
    exists = true,
    hexi_type = hexi_type,
    position = {0, 0},  -- filler value
    reset_column = column,
    reset_layer = floor,
    orientation = orientation,
    is_stable = false,
    should_lock = false,
  }

  setmetatable(obj, {__index=Hexi})
  obj:reset(obj.hexi_type)  -- Actually setting up hexi's position
  return obj
end

---Whether `bitmap_to_check` has no collisions with `matrix` with given position
---
---Position uses board coordinate.
---@param shape_to_check boolean[][] The `shape` of hexi to be checked
---@param pos_x integer x offset of the `shape`
---@param pos_y integer y offset of the `shape`, hexi coordinate system
---@param collision_matrix boolean[][] The collisions matrix of the `Board`
---@return boolean
local function has_no_collision(shape_to_check, pos_x, pos_y, collision_matrix)
  for y, row in ipairs(shape_to_check) do
    for x, piece in ipairs(row) do
      if not piece or y + pos_y > #collision_matrix then
        goto continue
      end
      local cell_to_check = collision_matrix[y + pos_y][x + pos_x]
      if cell_to_check then
        return false
      end
      ::continue::
    end
  end
  return true
end

---Update position and update next_update_move after that
---@param matrix integer[][]
---@also_perform_check_before_update boolean
function Hexi:update(matrix, also_perform_check_before_update)
  if also_perform_check_before_update then
    local offset_x = self.position[1]
    local offset_y = self.position[2]
    offset_y = math.ceil(offset_y / 2)

    if has_no_collision(self.shape, offset_x, offset_y + 1, matrix) then
      self.next_update_move = 2
    elseif has_no_collision(self.shape, offset_x + 1, offset_y, matrix) then
      self.next_update_move = 3
    elseif has_no_collision(self.shape, offset_x - 1, offset_y + 1, matrix) then
      self.next_update_move = 1
    else
      self.next_update_move = 0
    end
  end

  if self.next_update_move == 2 then
    self.position[2] = self.position[2] + 2
  end
  if self.next_update_move == 3 then
    self.position[1] = self.position[1] + 1
  end
  if self.next_update_move == 1 then
    self.position[2] = self.position[2] + 2
    self.position[1] = self.position[1] - 1
  end

  local offset_x = self.position[1]
  local offset_y = self.position[2]
  offset_y = math.ceil(offset_y / 2)

  if has_no_collision(self.shape, offset_x, offset_y + 1, matrix) then
    self.next_update_move = 2
  elseif has_no_collision(self.shape, offset_x + 1, offset_y, matrix) then
    self.next_update_move = 3
  elseif has_no_collision(self.shape, offset_x - 1, offset_y + 1, matrix) then
    self.next_update_move = 1
  else
    self.next_update_move = 0
  end
end

---Assign or extend shape and assign position, type to this hexi and update self.next_update_move accordingly.
---When this hexi already exists, the old position is kept
---@param matrix boolean[][]
---@param shape boolean[][]
---@param position halfVec2 Hexi coordinate system (y position is devided by 2, rounded up)
---@param type_id integer
function Hexi:assign(matrix, shape, position, type_id)
  self.exists = true
  self.shape = shape
  self.position = position
  self.type_id = type_id

  local offset_x = self.position[1]
  local offset_y = self.position[2]
  offset_y = math.ceil(offset_y / 2)

  if has_no_collision(self.shape, offset_x, offset_y + 1, matrix) then
    self.next_update_move = 2
  elseif has_no_collision(self.shape, offset_x + 1, offset_y, matrix) then
    self.next_update_move = 3
  elseif has_no_collision(self.shape, offset_x - 1, offset_y + 1, matrix) then
    self.next_update_move = 1
  else
    self.next_update_move = 0
  end
end

---Whether the position of this hexi is valid
---@param position halfVec2
---@param matrix integer[][]
---@param is_position_absolute boolean? If true, parameter `position` acts as position of this hexi. Default to false
---@return boolean
function Hexi:can_exist_as(position, matrix, is_position_absolute)
  local pos_x, pos_y
  if not is_position_absolute then
    pos_y = position[1] + self.position[1]
    pos_x = position[2] + self.position[2]
  else
    pos_x = position[1]
    pos_y = position[2]
  end

  if pos_y % 2 == 1 then
    return has_no_collision(self.shape, pos_x, math.ceil(pos_y/2), matrix)  -- aligned with the grid since the grid is one-indexed
  end
  return has_no_collision(self.shape, pos_x, pos_y / 2, matrix) and has_no_collision(self.shape, pos_x, pos_y / 2 + 1, matrix)
end

---Whether the position of this hexi is valid
---@param position halfVec2
---@param matrix integer[][]
---@param is_position_absolute boolean? If true, parameter `position` acts as position of this hexi. Default to false
---@return boolean
function PlayerControlledHexi:can_exist_as(position, matrix, is_position_absolute)
  local pos_x, pos_y
  if not is_position_absolute then
    pos_y = position[1] + self.position[1]
    pos_x = position[2] + self.position[2]
  else
    pos_x = position[1]
    pos_y = position[2]
  end

  local bitmap_to_check = self.hexi_type.rotation_table[self.orientation].bitmap

  if pos_y % 2 == 1 then
    return has_no_collision(bitmap_to_check, pos_x, math.ceil(pos_y/2), matrix)  -- aligned with the grid since the grid is one-indexed
  end
  return has_no_collision(bitmap_to_check, pos_x, pos_y / 2, matrix) and has_no_collision(bitmap_to_check, pos_x, pos_y / 2 + 1, matrix)
end

---Move this Hexi `direction` cells. Return false if the move cannot be done.
---@param direction integer Positive to go right
---@param matrix integer[]
function PlayerControlledHexi:move(direction, matrix)
  if direction == 0 then
    return true
  end

  local is_specified_move_valid = true
  local vector_to_origin = direction > 0 and -1 or 1
  while true do
    if self:can_exist_as({direction, 0}, matrix) then
      break
    end
    is_specified_move_valid = false
    direction = direction + vector_to_origin
  end

  self.position[1] = self.position[1] + direction
  self:update_status(matrix)
  return is_specified_move_valid
end

function PlayerControlledHexi:soft_drop(matrix)
  if self:can_exist_as({0, 1}, matrix) then
    self.position[2] = self.position[2] + 1
  end
  self:update_status(matrix)
end

---Update self.should_lock and self.is_stable
---@param matrix integer[][]
function PlayerControlledHexi:update_status(matrix)
  self.is_stable = false
  self.should_lock = false

  local bitmap = self.hexi_type.rotation_table[self.orientation].bitmap
  local offset_x = self.position[1]
  local offset_y = math.ceil(self.position[2] / 2)

  if not has_no_collision(bitmap, offset_x, offset_y + 1, matrix) then
    self.is_stable = true
    if not has_no_collision(bitmap, offset_x + 1, offset_y, matrix) and
      not has_no_collision(bitmap, offset_x - 1, offset_y + 1, matrix) then
      self.should_lock = true
    end
  end

end

---Rotate this Hexi
---@param rotation integer Positive to rotate clockwise
---@param matrix integer[][]
---@return boolean
function PlayerControlledHexi:rotate(rotation, matrix)
  if rotation == 0 then
    return true
  end

  local orientation = (self.orientation + rotation - 1) % 6 + 1
  local bitmap = self.hexi_type.rotation_table[orientation].bitmap
  local tests = self.hexi_type.rotation_table[orientation].rotation_tests

  if has_no_collision(bitmap, self.position[1], self.position[2], matrix) then
    self.orientation = orientation
    return true
  end

  for vec in tests do
    if has_no_collision(bitmap, self.position[1] + vec[1], self.position[2] + vec[2], matrix) then
      self.orientation = orientation
      self.position[1] = self.position[1] + vec[1]
      self.position[2] = self.position[2] + vec[2]
      self:update_status(matrix)
      return true
    end
  end

  return false
end

---Move this hexi to the top of the screen and change type to `hexi_type`
---
---Return shape, position, and type_id of the old states
---@param hexi_type HexiType
---@param orientation integer
---@return boolean[][] shape
---@return { [1]: integer, [2]: integer } position
---@return integer type_id
function PlayerControlledHexi:reset(hexi_type, orientation)
  orientation = orientation or 1

  local old_shape = self.hexi_type.rotation_table[self.orientation].bitmap
  local old_position = self.position
  local old_type_id = self.hexi_type.id

  self.exists = true
  self.is_stable = false
  self.should_lock = false
  self.orientation = 1
  self.hexi_type = hexi_type

  local hexi_lowest_position = hexi_type.rotation_table[orientation].graph_lowset_position
  local hexi_center_column = hexi_type.rotation_table[orientation].graph_center_column
  self.position[1] = self.reset_column - hexi_center_column + 1
  self.position[2] = self.reset_layer[self.position[1]] - hexi_lowest_position[2] + 1

  return old_shape, old_position, old_type_id
end

---@param matrix integer[]
---@return vec2
function PlayerControlledHexi:get_lowest_position(matrix)
  local position = {}  -- semi-deep copy
  position[1] = self.position[1]
  position[2] = self.position[2]
  while true do
    local bitmap = self.hexi_type.rotation_table[self.orientation].bitmap
    local offset_x = position[1]
    local offset_y = position[2]
    offset_y = math.ceil(offset_y / 2)

    if has_no_collision(bitmap, offset_x, offset_y + 1, matrix) then
      position[2] = position[2] + 1
    elseif has_no_collision(bitmap, offset_x + 1, offset_y, matrix) then
      position[1] = position[1] + 1
    elseif has_no_collision(bitmap, offset_x - 1, offset_y + 1, matrix) then
      position[2] = position[2] + 1
      position[1] = position[1] - 1
    else
      break
    end
  end
  return position
end

