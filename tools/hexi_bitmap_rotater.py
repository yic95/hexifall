def bitmap_rotate_varient(matrix: list) -> list:
    result = []
    result.append(matrix)
    for _ in range(5):
        x_max = len(result[-1]) - 1
        y_max = x_max + len(result[-1][0]) - 1
        prepared_bitmap = [[False for _ in range(x_max + 1)] for _ in range(y_max + 1)]
        for y, row in enumerate(result[-1]):
            for x, cell in enumerate(row):
                prepared_bitmap[x+y][x_max-y] = cell
        while True:
            if sum(prepared_bitmap[0]) != 0:
                break
            prepared_bitmap.pop(0)
        while True:
            if sum(prepared_bitmap[-1]) != 0:
                break
            prepared_bitmap.pop()
        result.append(prepared_bitmap)
    return result


def main():
    pass


if __name__ == '__main__':
    main()
