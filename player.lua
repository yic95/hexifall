-- require("hexi")
-- require("board")

---@alias action_hard_drop boolean
---@alias action_soft_drop boolean
---@alias action_rotate {[1]: boolean, [2]: boolean, [3]: boolean}
---@alias action_move_side {[1]: boolean, [2]: boolean}
---@alias PlayerAction {[1]: action_hard_drop, [2]: action_soft_drop, [3]: action_rotate, [4]: action_move_side}

local function concat_array(array1, array2)
  for i = 1, #array2 do
    array1[#array1+1] = array2[i]
  end
  return array1
end

local function shuf_array(array)
  for i = 1, #array do
    math.randomseed(os.clock())
    local randnum = math.random(#array)
    local t = array[randnum]
    array[randnum] = array[i]
    array[i] = t
  end
  return array
end

---Update Board and manipulate player-controlled hexies while keeping timing right
---@class Player
---@field board Board
---@field player_controlled_hexi PlayerControlledHexi
---@field hexi_generator HexiGenerator
---@field next_queue HexiType[]
---@field next_queue_top integer
---@field next_queue_length integer
---@field force_stablize_countdown_timer number
---@field force_stablize_countdown_timer_cap number
---@field move_axie AutoRepeatAxie
---@field soft_drop_axie AutoRepeatAxie
---@field action_buttons RisingEdgeButton[] Hard drop, rotate cw, rotate ccw, rotate 180deg
---@field gravity_tick_timer number
---@field gravity_tick_period number
Player = {}

---@param id integer
---@param hexi_generator HexiGenerator
---@return Player
function Player:new(id, hexi_generator)
  local obj = {}
  obj.board = Board:new(id, 11, 20)
  setmetatable(obj, {__index=Player})
  return obj
end

---Add `hexi` to the queue and pop the oldest hexi out. Return the hexi popped out
---@param hexi HexiType
---@return HexiType top
function Player:queue_next(hexi)
  local result = self.next_queue[self.next_queue_top]
  self.next_queue[(self.next_queue_top + self.next_queue_length - 1) % #self.next_queue + 1] = hexi
  self.next_queue_top = (self.next_queue_top) % #self.next_queue + 1
  return result
end

function Player:queue_peek()
  local result = {}
  for x = 1, self.next_queue_length do
    result[#result+1] = self.next_queue[(self.next_queue_top + x - 2) % #self.next_queue + 1]
  end
  return result
end

---Perform a tick
---@param dt number Delta time
function Player:tick(dt, player_input)
  local do_soft_drop = self.soft_drop_axie:tick(dt, player_input[2], false)
  local do_hard_drop = self.action_buttons[1]:tick(player_input[1])
  local do_side_movement = 0
  local side_movement_button_status = self.move_axie:tick(dt, player_input[4][1], player_input[4][2])
  if side_movement_button_status == false then do_side_movement = 0 end
  if side_movement_button_status == 1 then do_side_movement = 1 end
  if side_movement_button_status == 2 then do_side_movement = -1 end
  local do_rotation = 0
  local rotate_button_status = {}
  for i = 1, 3 do
    rotate_button_status[i] = self.action_buttons[i + 1]:tick(player_input[3][i])
  end
  if rotate_button_status[1] then do_rotation = do_rotation + 1 end
  if rotate_button_status[2] then do_rotation = do_rotation - 1 end
  if rotate_button_status[3] then do_rotation = do_rotation + 3 end

  local old_stable_status = self.player_controlled_hexi.is_stable
  local old_lock_status = self.player_controlled_hexi.should_lock
  local do_gravity_tick = true

  if do_hard_drop then
    local lowest_position = self.player_controlled_hexi:get_lowest_position(self.board.matrix)
    local shape, _, type_id = self.player_controlled_hexi:reset(self:queue_next(self.hexi_generator:generate_hexi()), 1)
    baord:add_hiexes(shape, lowest_position, type_id)
    self.soft_drop_axie:reset()
    self.move_axie:reset()
    self.gravity_tick_timer = 0
    do_gravity_tick = false
  elseif do_soft_drop then
    self.player_controlled_hexi:soft_drop(self.board.matrix)
    if old_stable_status then  -- if the hexi is already stable, we should stablize the hexi
      local shape, position, type_id = self.player_controlled_hexi:reset(self:queue_next(self.hexi_generator:generate_hexi()), 1)
      if old_lock_status then
        self.board:add_hiexes(shape, position, type_id)
      else
        self.board:add_self_moving_hexi(shape, position, type_id)
      end
      do_gravity_tick = false
    end
  else
    self.player_controlled_hexi:move(do_side_movement, self.board.matrix)
    self.player_controlled_hexi:rotate(do_rotation, self.board.matrix)
  end

  if do_gravity_tick then
    self.gravity_tick_timer = self.gravity_tick_timer + dt
    if self.gravity_tick_timer > self.gravity_tick_period then
      self.gravity_tick_timer = self.gravity_tick_timer - self.gravity_tick_period
      self.board:update()
      if not do_soft_drop then  -- to keep soft drop speed consistent
        self.player_controlled_hexi:soft_drop(self.board.matrix)
      end
    end
    if not self.player_controlled_hexi.is_stable then
      self.force_stablize_countdown_timer = self.force_stablize_countdown_timer_cap
    end
    if self.player_controlled_hexi.is_stable then
      self.force_stablize_countdown_timer = self.force_stablize_countdown_timer - dt
    end
    if self.force_stablize_countdown_timer < 0 then
      old_lock_status = self.player_controlled_hexi.should_lock
      local shape, position, type_id = self.player_controlled_hexi:reset(self:queue_next(self.hexi_generator:generate_hexi()), 1)
      if old_lock_status then
        self.board:add_hiexes(shape, position, type_id)
      else
        self.board:add_self_moving_hexi(shape, position, type_id)
      end
    end
  end
end

---@class HexiGenerator
---@field bag_size integer
---@field bag HexiType[]
---@field bag_next_index integer Index to `bag`. HexiType of this index would be retuned by self.generate_hexi after calling.
---@field bag_based_shuffle
---| true: Bag-based shuffler
---| false: Bagless shuffler (fully random)
HexiGenerator = {}

---Constructor
---@param hexi HexiType[] Types of hexies that are allowed to be generated
---@param bag_size_mult
---| 0  # Fully random, bagless hexi generation
---| 1 # The default value
---| integer # Bag size multiplier
---@return HexiGenerator
function HexiGenerator:new(hexi, bag_size_mult)
  bag_size_mult = bag_size_mult and bag_size_mult or 1
  local is_bagbased_gen = true
  if bag_size_mult == 0 then
    is_bagbased_gen = false
    bag_size_mult = 1
  end

  local bag = hexi
  local bag_size = bag_size_mult * #hexi
  if is_bagbased_gen then
    while bag_size_mult > 1 do
      concat_array(bag, hexi)
      bag_size_mult = bag_size_mult - 1
    end
    shuf_array(bag)
  end

  local obj = {
    bag_size = bag_size,
    bag = bag,
    bag_next_index = 1,
  }
  setmetatable(obj, {__index=HexiGenerator})
  return obj
end

---Generate HexiType
---@return HexiType
function HexiGenerator:generate_hexi()
  if self.bag_based_shuffle then
    local result = self.bag[self.bag_next_index]
    self.bag_next_index = self.bag_next_index + 1
    if self.bag_next_index > self.bag_size then
      shuf_array(self.bag)
      self.bag_next_index = 1
    end
    return result
  else
    math.randomseed(os.clock())
    return self.bag[math.random(#self.bag)]
  end
end

---@class AutoRepeatAxie
---@field auto_repeat_interval number
---@field auto_repeat_interval_initial number
---@field timer number
---@field previous_input_status {[1]: boolean, [2]: boolean}
---@field is_initial_repeat boolean
---@field repeating_key 0|1|2
AutoRepeatAxie = {}

---@param delay number
---@param initial_delay number
---@return AutoRepeatAxie
function AutoRepeatAxie:new(delay, initial_delay)
  local obj = {
    auto_repeat_interval = delay,
    auto_repeat_interval_initial = initial_delay,
    timer = 0,
    previous_input_status = {false, false},
    is_initial_repeat = false,
    repeating_key = 0
  }

  setmetatable(obj, {__index=AutoRepeatAxie})
  return obj
end

---@param dt number
---@param is_button1_down boolean
---@param is_button2_down boolean
---@return 1|2|false
function AutoRepeatAxie:tick(dt, is_button1_down, is_button2_down)
  self.timer = self.timer + dt

  local result = false  ---@type 1|2|false
  -- Key first pressed
  if not self.previous_input_status[1] and is_button1_down then
    self.is_initial_repeat = true
    self.timer = 0
    self.repeating_key = 1
    result = 1
  elseif not self.previous_input_status[2] and is_button2_down then
    self.is_initial_repeat = true
    self.timer = 0
    self.repeating_key = 2
    result = 2
  end

  local is_timer_overflow = self.timer > (self.is_initial_repeat and self.auto_repeat_interval_initial or self.auto_repeat_interval)
  if is_timer_overflow then
    self.timer = 0
  end

  if is_timer_overflow and is_button1_down and self.repeating_key == 1 then
    self.is_initial_repeat = false
    result = 1
  elseif is_timer_overflow and is_button2_down and self.repeating_key == 2 then
    self.is_initial_repeat = false
    result = 2
  end

  if not is_button1_down and self.repeating_key == 1 then
    self.repeating_key = 0
  end
  if not is_button2_down and self.repeating_key == 2 then
    self.repeating_key = 0
  end

  self.previous_input_status = {is_button1_down, is_button2_down}

  return result
end

function AutoRepeatAxie:reset()
  self.previous_input_status = {false, false}
  self.is_initial_repeat = true
  self.timer = 0
  self.repeating_key = 0
end

---@class RisingEdgeButton
---@field previous_input_status boolean
RisingEdgeButton = {}

function RisingEdgeButton:new()
  local obj = {
    previous_input_status = false
  }
  setmetatable(obj, {__index=RisingEdgeButton})
end

function RisingEdgeButton:tick(is_button_down)
  local result = false
  if is_button_down and self.previous_input_status == false then
    result = true
  end
  self.previous_input_status = is_button_down

  return result
end

function RisingEdgeButton:reset()
  self.previous_input_status = false
end

